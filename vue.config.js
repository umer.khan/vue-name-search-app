module.exports = {
    baseUrl: process.env.NODE_ENV === 'production'
        ? '/vue-name-search-app/'
        : '/'
}